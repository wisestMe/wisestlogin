﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginApp.WebSite
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Webform1.aspx");
        }
    }
}