﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="LoginApp.WebSite.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Registration Page</h1>
            <%--<p>
                <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" ContinueDestinationPageUrl="~/WebForm1.aspx">
                    <WizardSteps>
                        <asp:CreateUserWizardStep runat="server" />
                        <asp:CompleteWizardStep runat="server" />
                    </WizardSteps>
                </asp:CreateUserWizard>
            </p>--%>

            <table>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text="Username"></asp:Label></td>
                    <td><asp:TextBox ID="newUsername" runat="server"></asp:TextBox></td> 
                </tr>

                <tr>
                    <td><asp:Label ID="Label2" runat="server" Text="Password"></asp:Label></td>
                    <td><asp:TextBox ID="newPassword1" runat="server"></asp:TextBox></td> 
                </tr>

                <tr>
                    <td><asp:Label ID="Label3" runat="server" Text="Confirm Password"></asp:Label></td>
                    <td><asp:TextBox ID="newPassword2" runat="server"></asp:TextBox></td> 
                </tr>

                <tr>
                    <td><asp:Label ID="Label4" runat="server" Text="E-mail"></asp:Label></td>
                    <td><asp:TextBox ID="email" runat="server"></asp:TextBox></td> 
                </tr>

                <tr>
                    <td><asp:Label ID="Label5" runat="server" Text="Security Question"></asp:Label></td>
                    <td><asp:TextBox ID="securityQuestion" runat="server"></asp:TextBox></td> 
                </tr>

                <tr>
                    <td><asp:Label ID="Label6" runat="server" Text="Security Answer"></asp:Label></td>
                    <td><asp:TextBox ID="securityAnswer" runat="server"></asp:TextBox></td>
                </tr>

                <tr>
                    <td></td>
                    <td><asp:Button ID="createAccount" runat="server" Text="Create Account" OnClick="createAccount_Click" /></td> 
                </tr>
                
            </table>
        </div>
        <p>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Home.aspx">Home</asp:HyperLink>
        </p>
    </form>
</body>
</html>
