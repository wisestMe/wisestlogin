﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace LoginApp.WebSite
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loginErrorMessage.Visible = false;

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\wisestMe\source\repos\LoginApp\LoginApp.WebSite\App_Data\UersDB.mdf;Integrated Security=True"))
            {
                connection.Open();
                string query = "SELECT COUNT(1) FROM DBUsers WHERE username=@username AND password=@password";
                SqlCommand instruction = new SqlCommand(query, connection);
                instruction.Parameters.AddWithValue("@username", textUsername.Text.Trim());
                instruction.Parameters.AddWithValue("@password", textPassword.Text.Trim());
                int count = Convert.ToInt32(instruction.ExecuteScalar());
                if (count == 1)
                {
                    Session["username"] = textUsername.Text.Trim();
                    Response.Redirect("Home.aspx");
                }
                else
                {
                    loginErrorMessage.Visible = true;
                }
            }
        }
    }
}